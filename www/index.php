<?php
/*
 * tux-translate - Automatic human language translation.
 * Copyright (C) 2007 Thomas Cort <tux-translate@tux.name>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

include_once('config.php');

$expected_url  = parse_url($base_url);	/* make sure we're on the correct page                        */
$expected_host = $expected_url["host"]; /* host name of page must match host name of SOAP service     */
$actual_host = $_SERVER["SERVER_NAME"]; /* if they don't match, the browser will block the connection */
					/* Firefox 2.0.0.9 even thinks 127.0.0.1 != localhost.        */
if ($expected_host != $actual_host) {	/* use every trick we know to redirect the browser...         */
	header("Location: " . $base_url);
	print("<html><head><title>Redirecting to " . $base_url . "</title>");
	print("<meta http-equiv=\"refresh\" content=\"1;URL=" . $base_url . "\">");
	print("</head><body>Redirecting to <a href=\"" . $base_url . "\">" . $base_url . "</a>");
	print("<script type=\"text/javascript\">window.location = " . $base_url . ";</script>");
	print("</body></html>");
	exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title>Tux Translate</title>
  <meta http-equiv="Content-Language" content="English" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="css/default.css" media="screen" />
  <script type="text/javascript" src="js/tt.php"></script>
 </head>
 <body>
  <div id="header">
   Welcome to Tux Translate
  </div>
  <div id="container">
    <div id="content">
     <table>
      <tr>
       <td>
        [<a href="<?php echo $base_url . "/code/tux-translate-0001.tar.gz"; ?>">code</a>]
        [<a href="http://repo.or.cz/w/tux-translate.git">git-repo</a>]
        [<a href="<?php echo "mailto:" . $admin_email; ?>">mail</a>]
        [<a href="<?php echo $base_url . "/soap.php"; ?>">soap</a>]
        [<a href="<?php echo $base_url . "/soap.wsdl"; ?>">wsdl</a>]
       </td>
      </tr>
     </table>
     <table>
      <tr>
       <td>
       <form action="javascript:translate()" id="tt">
        <fieldset> 
         <p>
          Source Language:
          <select name="srclang">
<option value="en">English</option>
<option value="af">Afrikaans</option>
<option value="be">Breton</option>
<option value="ca">Catalan</option>
<option value="cs">Czech</option>
<option value="da">Danish</option>
<option value="de">German</option>
<option value="el">Greek</option>
<option value="en">English</option>
<option value="eo">Esperanto</option>
<option value="es">Spanish</option>
<option value="et">Estonian</option>
<option value="eu">Basque</option>
<option value="fi">Finnish</option>
<option value="fr">French</option>
<option value="ga">Irish</option>
<option value="gl">Gallegan</option>
<option value="hu">Hungarian</option>
<option value="id">Indonesian</option>
<option value="it">Italian</option>
<option value="ja">Japanese</option>
<option value="ko">Korean</option>
<option value="ms">Malay</option>
<option value="nl">Dutch</option>
<option value="pl">Polish</option>
<option value="pt">Portuguese</option>
<option value="ro">Romanian</option>
<option value="ru">Russian</option>
<option value="sk">Slovak</option>
<option value="sl">Slovenian</option>
<option value="sr">Serbian</option>
<option value="sv">Swedish</option>
<option value="tr">Turkish</option>
<option value="uk">Ukrainian</option>
<option value="vi">Vietnamese</option>
<option value="zh_CN">Simplified Chinese</option>
<option value="zh_TW">Traditional Chinese</option>
          </select>
         </p>
         <p>
         Target Language:
          <select name="dstlang">
<option value="fr">French</option>
<option value="af">Afrikaans</option>
<option value="be">Breton</option>
<option value="ca">Catalan</option>
<option value="cs">Czech</option>
<option value="da">Danish</option>
<option value="de">German</option>
<option value="el">Greek</option>
<option value="en">English</option>
<option value="eo">Esperanto</option>
<option value="es">Spanish</option>
<option value="et">Estonian</option>
<option value="eu">Basque</option>
<option value="fi">Finnish</option>
<option value="fr">French</option>
<option value="ga">Irish</option>
<option value="gl">Gallegan</option>
<option value="hu">Hungarian</option>
<option value="id">Indonesian</option>
<option value="it">Italian</option>
<option value="ja">Japanese</option>
<option value="ko">Korean</option>
<option value="ms">Malay</option>
<option value="nl">Dutch</option>
<option value="pl">Polish</option>
<option value="pt">Portuguese</option>
<option value="ro">Romanian</option>
<option value="ru">Russian</option>
<option value="sk">Slovak</option>
<option value="sl">Slovenian</option>
<option value="sr">Serbian</option>
<option value="sv">Swedish</option>
<option value="tr">Turkish</option>
<option value="uk">Ukrainian</option>
<option value="vi">Vietnamese</option>
<option value="zh_CN">Simplified Chinese</option>
<option value="zh_TW">Traditional Chinese</option>
          </select>
         </p>
         <p>
          <input type="text" name="msg" id="msg" autocomplete="off" alt="msg" onkeyup="suggest()"/>
          <input type="submit" value="Translate Now!"/><br/>
          <div id="suggest">
          </div>
         </p>
        </fieldset> 
       </form>
       </td>
      </tr>
     </table>
    </div>
  </div>
  <div id="results">
  </div>
 </body>
</html>
