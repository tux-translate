/*
 * tux-translate - Automatic human language translation.
 * Copyright (C) 2007 Thomas Cort <tux-translate@tux.name>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

<?php include_once('../config.php'); ?>


var barid;
var barrunning;

function hitProgressBar(id, x) {
	clearTimeout(barid);

	if (barrunning == 1) {

		var resultsdiv = document.getElementById(id);
		resultsdiv.firstChild.style.backgroundColor = '#47006b';

		var cells = resultsdiv.getElementsByTagName("td"); 

		if ((x-10) >= cells.length) {
			x = 0;
		}

		for (var i = 0; i < cells.length; i++) {
			if (i <= x && i >= (x-10)) {
				cells[i].style.backgroundColor = '#d580ff';
			} else {
				cells[i].style.backgroundColor = '#eabfff';
			}
		}

		x = x + 1;
		barid = setTimeout('hitProgressBar("results",' + x + ')', 100);
	}
}

function addProgressBar(id) {
	barrunning = 1;
	document.getElementById(id).innerHTML = '<table><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>';
	barid = setTimeout('hitProgressBar("results",1)', 10);
}

function clearresult() {
	document.getElementById("results").innerHTML = "";
}

function translateResponse(response, call, status, last) {
	clearTimeout(barid);
	barrunning = 0;

	suggestClear();

	var resultHTML = "<table><tr><td><form action=\"javascript:clearresult()\" id=\"resultform\"><fieldset>";

	if (response.fault) {
		resultHTML += "<p><b><i>[Server Error]</i></b><br/>";
		if (response.fault.faultString) {
			resultHTML += "<tt>" + response.fault.faultString + "</tt>";
		}
		resultHTML += "</p>";
	} else {

		var namecnt     = response.body.getElementsByTagName("name").length;
		var versioncnt  = response.body.getElementsByTagName("version").length;
		var homepagecnt = response.body.getElementsByTagName("homepage").length;
		var downloadcnt = response.body.getElementsByTagName("download").length;
		var messagecnt  = response.body.getElementsByTagName("message").length;
		var licnamecnt  = response.body.getElementsByTagName("license-name").length;
		var licurlcnt   = response.body.getElementsByTagName("license-url").length;

		if (namecnt != 0 && versioncnt != 0 && homepagecnt != 0 && downloadcnt != 0 && messagecnt != 0 && licnamecnt != 0 && licurlcnt != 0) {
			var name     = response.body.getElementsByTagName("name")[0].childNodes[0].nodeValue;
			var version  = response.body.getElementsByTagName("version")[0].childNodes[0].nodeValue;
			var homepage = response.body.getElementsByTagName("homepage")[0].childNodes[0].nodeValue;
			var download = response.body.getElementsByTagName("download")[0].childNodes[0].nodeValue;
			var message  = response.body.getElementsByTagName("message")[0].childNodes[0].nodeValue;
			var licname  = response.body.getElementsByTagName("license-name")[0].childNodes[0].nodeValue;
			var licurl   = response.body.getElementsByTagName("license-url")[0].childNodes[0].nodeValue;

			resultHTML += "<p><b><i>" + message + "</i></b></p>";
			resultHTML += "<p>Package: <a href=\"" + homepage + "\">" + name + "</a> " + version + " (<a href=\"" + download + "\">source</a>)<br/>";
			resultHTML += "<a href=\"" + licurl + "\">" + licname + "</a></p>";
		} else {
			resultHTML += "<p><b><i>[Translation Not Found]</i></b></p>";
		}

	}

	resultHTML += "<p><input type=\"submit\" value=\"Clear\"/></p>";
	resultHTML += "</fieldset></form></td></tr></table>";

	// Display the results
	document.getElementById("results").innerHTML = resultHTML;

	return;
}

function translate() {
	var soapURI = "<?php echo $base_url; ?>/soap.php";
	var mySOAPCall = new SOAPCall();
	var params = new Array();
	var frm = document.getElementById("tt");

	suggestClear();
	addProgressBar("results");

	params[0] = new SOAPParameter();
	params[0].name = "source-language";
	params[0].value = frm.srclang.value;

	params[1] = new SOAPParameter();
	params[1].name = "target-language";
	params[1].value = frm.dstlang.value;

	params[2] = new SOAPParameter();
	params[2].name = "message";
	params[2].value = frm.msg.value;

	mySOAPCall.transportURI = soapURI;
	mySOAPCall.encode(SOAPCall.VERSION_1_1, "Translate", "urn:http://www.tux.name/tt", 0, null, params.length, params);
	mySOAPCall.asyncInvoke(translateResponse);

	return;
}

var suggestInProgress = 0;

function suggestResponse(response, call, status, last) {
	var s = document.getElementById('suggest');
	s.innerHTML = '';

	if (response.fault) {
		suggestInProgress = 0;
		return;
	} else {
		var suggestions = response.body.getElementsByTagName("message");

		if (suggestions.length == 1 && document.getElementById('msg').value == suggestions[0].childNodes[0].nodeValue) {
			suggestInProgress = 0;
			return;
		}

		for (var i = 0; i < suggestions.length; i++) {
			s.innerHTML += '<div onmouseover="javascript:suggestOver(this);" onmouseout="javascript:suggestOff(this);" onclick="javascript:suggestChoose(this.innerHTML);" class="suggest_link">' + suggestions[i].childNodes[0].nodeValue + '</div>';
		}
	}

	suggestInProgress = 0;
}

function suggest() {
	if (suggestInProgress) {
		return;
	}

	suggestInProgress = 1;

	var soapURI = "<?php echo $base_url; ?>/soap.php";
	var mySOAPCall = new SOAPCall();
	var params = new Array();

	params[0] = new SOAPParameter();
	params[0].name = "language";
	params[0].value = document.getElementById("tt").srclang.value;

	params[1] = new SOAPParameter();
	params[1].name = "partial-message";
	params[1].value = document.getElementById('msg').value;

	mySOAPCall.transportURI = soapURI;
	mySOAPCall.encode(SOAPCall.VERSION_1_1, "Suggest", "urn:http://www.tux.name/tt", 0, null, params.length, params);
	mySOAPCall.asyncInvoke(suggestResponse);

	return;
}


function suggestOver(choice) {
	choice.className = 'suggest_choice_over';
}

function suggestOff(choice) {
	choice.className = 'suggest_choice';
}

function suggestChoose(choice) {
	document.getElementById('msg').value = choice;
	document.getElementById('suggest').innerHTML = '';
}

function suggestClear() {
	document.getElementById('suggest').innerHTML = '';
}
