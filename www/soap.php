<?php
/*
 * tux-translate - Automatic human language translation.
 * Copyright (C) 2007 Thomas Cort <tux-translate@tux.name>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

include_once('./config.php');
include_once('./lib/db.php');

function Suggest($language, $partial_message) {
	db_connect();
	/* do some db calls here to get the info we need   */
	$result = db_suggest($language, $partial_message);
	db_disconnect();

	if ($result) {
		return $result;
	} else {
		/* No suggestions found :( */
		return array();
	}
}

function AddTranslation($auth, $package, $source_language, $source_message, $target_language, $target_message) {

	return array("success" => true);
}

function Translate($source_language, $target_language, $message) {
	global $base_url;

	db_connect();
	if ($source_language != "en" && $target_language != "en") {
		$tmp = db_translate($source_language, "en", $message);
		if ($tmp) {
			$result = db_translate("en", $target_language, $tmp["message"]);
		} else {
			$result = false;
		}
	} else {
		$result = db_translate($source_language, $target_language, $message);
	}
	db_disconnect();

	if ($result) {
		return array(
			"package" => array(
				"name" => $result["name"],
				"version" => $result["version"],
				"homepage" => $result["homepage"],
				"download" => $base_url . $result["download"],
				"license" => array(
					"license-name" => $result["licname"],
					"license-url" => $base_url . $result["licurl"]
				)
			),
			"message" => $result["message"]
		);
	} else {
		/* Translation not found :( */
		return array();
	}
}

require_once('./lib/nusoap.php');

$server = new nusoap_server('soap.wsdl');

if (isset($HTTP_RAW_POST_DATA)) {
	$server->service($HTTP_RAW_POST_DATA);
} else {
	$server->service('');
}
?>
