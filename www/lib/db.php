<?php
/*
 * tux-translate - Automatic human language translation.
 * Copyright (C) 2007 Thomas Cort <tux-translate@tux.name>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

$db_down_fault = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ><soap:Body><soap:Fault><faultcode>soap:Server</faultcode><faultstring>Database is unavailable.</faultstring><detail/></soap:Fault></soap:Body></soap:Envelope>
XML;

$db_unknown_lang_fault = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ><soap:Body><soap:Fault><faultcode>soap:Server</faultcode><faultstring>Unknown source and/or target language.</faultstring><detail/></soap:Fault></soap:Body></soap:Envelope>
XML;

$db_same_lang_fault = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ><soap:Body><soap:Fault><faultcode>soap:Server</faultcode><faultstring>Source language is the same as target language. No translation needed/attempted.</faultstring><detail/></soap:Fault></soap:Body></soap:Envelope>
XML;

$db_no_msg_fault = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ><soap:Body><soap:Fault><faultcode>soap:Server</faultcode><faultstring>No message to translate.</faultstring><detail/></soap:Fault></soap:Body></soap:Envelope>
XML;

/**
 * Connects to the database using the parameters from config.php
 */
function db_connect() {
	global $db_host;
	global $db_user;
	global $db_pass;
	global $db_name;
	global $db_conn;
	global $db_down_fault;

	if ($db_conn != false) {
		return; /* already connected */
	}

	if (!is_string($db_host) || !is_string($db_user) || !is_string($db_pass) || !is_string($db_name)) {
		die ($db_down_fault);
	}

	$db_conn = mysql_connect($db_host, $db_user, $db_pass) or die($db_down_fault);
	mysql_select_db($db_name) or die($db_down_fault);

}

/**
 * Closes the current database session
 */
function db_disconnect() {
	global $db_conn;

	mysql_close($db_conn);
	$db_conn = false;
}

/**
 * Checks if a database exists
 */
function db_table_exists($tbl) {
	$sql = "SHOW TABLES LIKE '" . mysql_real_escape_string($tbl) . "';";

	$rs = mysql_query($sql);
	if (!$rs) {
		die($db_down_fault);
	}
	
	if (1 == mysql_numrows($rs)) {
		$row = mysql_fetch_row($rs);
		$result = true;
	} else {
		$result = false;
	}

	mysql_free_result($rs);

	return $result;
}

/**
 * Translates a message from one language to another
 */
function db_translate($source_language, $target_language, $message) {
	global $db_down_fault;
	global $db_unknown_lang_fault;
	global $db_same_lang_fault;
	global $db_no_msg_fault;

	if ($message == "") {
		die($db_no_msg_fault);
	}

	if ($source_language == $target_language) {
		die($db_same_lang_fault);
	}

	if (!db_table_exists($source_language) || !db_table_exists($target_language)) {
		die($db_unknown_lang_fault);
	}

	/* Example Query:
	 * SELECT es.message AS message, package.name AS name, package.version AS version, package.homepage
	 * AS homepage, package.download AS download, license.name AS licname, license.url AS licurl FROM
	 * en INNER JOIN en_es ON en.id = en_es.ida INNER JOIN es ON en_es.idb = es.id INNER JOIN pkg_es ON
	 * pkg_es.langid = es.id INNER JOIN package ON pkg_es.pkgid = package.id INNER JOIN package_license
	 * ON package.id = package_license.pkgid INNER JOIN license ON package_license.licid = license.id
	 * WHERE en.message = 'at most one input file allowed';
	 */
	$sql = "SELECT " . mysql_real_escape_string($target_language) . ".message AS message, package.name AS name, package.version AS version, package.homepage AS homepage, package.download AS download, license.name AS licname, license.url AS licurl FROM " . mysql_real_escape_string($source_language) . " INNER JOIN " . mysql_real_escape_string($source_language) . "_" . mysql_real_escape_string($target_language) . " ON " . mysql_real_escape_string($source_language) . ".id = " . mysql_real_escape_string($source_language) . "_" . mysql_real_escape_string($target_language) . ".ida INNER JOIN " . mysql_real_escape_string($target_language) . " ON " . mysql_real_escape_string($source_language) . "_" . mysql_real_escape_string($target_language) . ".idb = " . mysql_real_escape_string($target_language) . ".id INNER JOIN pkg_" . mysql_real_escape_string($target_language) . " ON pkg_" . mysql_real_escape_string($target_language) . ".langid = " . mysql_real_escape_string($target_language) . ".id INNER JOIN package ON pkg_" . mysql_real_escape_string($target_language) . ".pkgid = package.id INNER JOIN package_license ON package.id = package_license.pkgid INNER JOIN license ON package_license.licid = license.id WHERE " . mysql_real_escape_string($source_language) . ".message LIKE '" . mysql_real_escape_string($message) . "';";

	$rs = mysql_query($sql);
	if (!$rs) {
		die($db_down_fault);
	}
	
	if (1 == mysql_numrows($rs)) {
		$row = mysql_fetch_row($rs);
		$result = array( "message" => $row[0], "name" => $row[1], "version" => $row[2], "homepage" => $row[3], "download" => $row[4], "licname" => $row[5], "licurl" => $row[6]);
	} else {
		$result = false;
	}

	mysql_free_result($rs);

	return $result;
}

function db_suggest($language, $partial_message) {
	global $db_down_fault;
	global $db_unknown_lang_fault;

	if (!db_table_exists($language)) {
		die($db_unknown_lang_fault);
	}

	$sql = "SELECT DISTINCT message FROM " . mysql_real_escape_string($language) . " WHERE message LIKE '" . mysql_real_escape_string($partial_message) . "%' LIMIT 10;";
	$rs = mysql_query($sql);
	if (!$rs) {
		die($db_down_fault);
	}

	$result = array();


	if (mysql_numrows($rs) > 0) {
		while ($tmp = mysql_fetch_array($rs)) {
			array_push($result, $tmp[0]);
		}
	} else {
		$result = false;
	}

	mysql_free_result($rs);

	return $result;
}

/**
 * Check if a message exists
 */
function db_message_present($msg, $lang) {
	global $db_down_fault;

	$sql = "SELECT (COUNT(message) > 0) AS message_present FROM " . mysql_real_escape_string($lang) . " WHERE message LIKE '" . mysql_real_escape_string($msg) . "'";

	$result = false;

	$rs = mysql_query($sql);
	if (!$rs) {
		die($db_down_fault);
	}

	if (1 == mysql_numrows($rs)) {
		$row = mysql_fetch_row($rs);
		if (mysql_real_escape_string($row[0]) == 1) {
			$result = true;
		}
	}

	mysql_free_result($rs);

	return $result;
}

/**
 * Check if a relationship exists
 */
function db_relationship_exists($langA, $langB, $idA) {
	global $db_down_fault;

	$sql = "SELECT (COUNT(*) > 0) AS relationship_present FROM " . mysql_real_escape_string($langA) . "_". mysql_real_escape_string($langB) . " WHERE ida = '" . mysql_real_escape_string($idA) . "'";

	$result = false;

	$rs = mysql_query($sql);
	if (!$rs) {
		die($db_down_fault);
	}

	if (1 == mysql_numrows($rs)) {
		$row = mysql_fetch_row($rs);
		if (mysql_real_escape_string($row[0]) == 1) {
			$result = true;
		}
	}

	mysql_free_result($rs);

	return $result;
}

/**
 * Get the id value of the package
 */
function db_get_package_id($name) {
	global $db_down_fault;

	$sql = "SELECT id FROM package WHERE name = '" . mysql_real_escape_string($name) . "';";

	$id = -1;

	$rs = mysql_query($sql);
	if (!$rs) {
		die($db_down_fault);
	}

	if (1 == mysql_numrows($rs)) {
		$row = mysql_fetch_row($rs);
		$id = mysql_real_escape_string($row[0]);
	}

	mysql_free_result($rs);

	return $id;
}

/**
 * Get the id value of the message
 */
function db_get_message_id($msg, $lang) {
	global $db_down_fault;

	$sql = "SELECT id FROM " . mysql_real_escape_string($lang) . " WHERE message LIKE '" . mysql_real_escape_string($msg) . "';";

	$id = -1;

	$rs = mysql_query($sql);
	if (!$rs) {
		die($db_down_fault);
	}

	if (1 == mysql_numrows($rs)) {
		$row = mysql_fetch_row($rs);
		$id = mysql_real_escape_string($row[0]);
	}

	mysql_free_result($rs);

	return $id;
}

?>
