<?php
/*
 * tux-translate - Automatic human language translation.
 * Copyright (C) 2007 Thomas Cort <tux-translate@tux.name>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

require_once('../www/lib/nusoap.php');

$soapurl    = "http://localhost/soap.php";
$soapaction = "http://www.tux.name/tt/AddTranslation";
$soapclient = new nusoap_client($soapurl);

$soapmsg = $soapclient->serializeEnvelope('
<AddTranslation xmlns="http://www.tux.name/tt">
        <auth>
                <username>user</username>
                <password>pass</password>
        </auth>
        <package>
                <name>hello</name>
                <version>2.3</version>
                <homepage>http://www.gnu.org/software/hello/</homepage>
                <download>http://localhost/distfiles/hello-2.3.tar.gz</download>
                <license>
                        <license-name>GNU General Public License v3 or Later</license-name>
                        <license-url>http://localhost/license/GPLv3orLater</license-url>
                </license>
        </package>
        <source-language>en</source-language>
        <source-message>hello, world</source-message>
        <target-language>fr</target-language>
        <target-message>Bonjour, le monde.</target-message>
</AddTranslation>
','',array(),'document', 'literal');


$result = $soapclient->send($soapmsg, $soapaction);

if ($soapclient->fault) {
	print_r($result);
} else {
	echo $soapclient->request . "\n";
	echo "---\n";
	echo $soapclient->response . "\n";
}
?>
