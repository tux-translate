<?php
/*
 * tux-translate - Automatic human language translation.
 * Copyright (C) 2007 Thomas Cort <tux-translate@tux.name>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * NOTICE: this is very much a work in progress and shouldn't be used in
 * production nor with translation files from unknown or unsafe sources.
 * The author only feels comfortable using it with software downloaded
 * directly from ftp://ftp.gnu.org/gnu to generate *test* data. Better,
 * safer, more secure tools that could be run with arbitrary data submitted
 * by users are on the way -- just be patient ;)
 */

include_once('../www/config.php');
include_once('../www/lib/db.php');

/**
 * Holds a translation. Maybe this could be generalized and used other places?
 */
class tt_translation {
	var $msgid;
	var $msgstr;
	var $locale;
}

/**
 * Inserts a translation into the database.
 */
function tt_insert_db($msgA, $langA, $msgB, $langB) {
	// ensure langA and langB tables exist
	$sql = "CREATE TABLE IF NOT EXISTS `" . mysql_real_escape_string($langA) . "` ( id MEDIUMINT NOT NULL AUTO_INCREMENT, message TEXT NOT NULL, PRIMARY KEY (id) );";
	mysql_query($sql);
	$sql = "CREATE TABLE IF NOT EXISTS `" . mysql_real_escape_string($langB) . "` ( id MEDIUMINT NOT NULL AUTO_INCREMENT, message TEXT NOT NULL, PRIMARY KEY (id) );";
	mysql_query($sql);

	// ensure langA_langB and langB_langA tables exist
	$sql = "CREATE TABLE IF NOT EXISTS `" . mysql_real_escape_string($langA) . "_" . mysql_real_escape_string($langB) . "` ( ida MEDIUMINT NOT NULL, idb MEDIUMINT NOT NULL, PRIMARY KEY (ida, idb) );";
	mysql_query($sql);
	$sql = "CREATE TABLE IF NOT EXISTS `" . mysql_real_escape_string($langB) . "_" . mysql_real_escape_string($langA) . "` ( ida MEDIUMINT NOT NULL, idb MEDIUMINT NOT NULL, PRIMARY KEY (ida, idb) );";
	mysql_query($sql);

	// ensure pkg_langA and pkg_langB tables exist
	$sql = "CREATE TABLE IF NOT EXISTS `pkg_" . mysql_real_escape_string($langA) . "` ( pkgid MEDIUMINT NOT NULL, langid MEDIUMINT NOT NULL, PRIMARY KEY (pkgid, langid) );";
	mysql_query($sql);
	$sql = "CREATE TABLE IF NOT EXISTS `pkg_" . mysql_real_escape_string($langB) . "` ( pkgid MEDIUMINT NOT NULL, langid MEDIUMINT NOT NULL, PRIMARY KEY (pkgid, langid) );";
	mysql_query($sql);

	// ensure msgA and msgB are not both in DB
	if (db_message_present($msgA, $langA) && db_message_present($msgB, $langB)) {
		return; /* translation already exists */
	}

	// ensure a slightly different translation for the same string doesn't already exist
	if (db_message_present($msgA, $langA)) {
		$idA = db_get_message_id($msgA, $langA);
		if ($idA == -1) {
			return;
		}

		if (db_relationship_exists($langA, $langB, $idA)) {
			return;
		}
	}

	if (db_message_present($msgB, $langB)) {
		$idB = db_get_message_id($msgB, $langB);
		if ($idB == -1) {
			return;
		}

		if (db_relationship_exists($langB, $langA, $idB)) {
			return;
		}
	}

	// get pkgid
	$pkgid = db_get_package_id("GNU gettext");
	if ($pkgid == -1) {
		return; /* package doesn't exist */
	}

	if (!db_message_present($msgA, $langA)) {
		// insert msgA
		$sql = "INSERT INTO " . mysql_real_escape_string($langA) . " (message) VALUES ('" . mysql_real_escape_string($msgA) . "');";
		mysql_query($sql);

		// get idA
		$idA = db_get_message_id($msgA, $langA);
		if ($idA == -1) {
			return;
		}

		// insert pkg_langA
		$sql = "INSERT INTO pkg_" . mysql_real_escape_string($langA) . " VALUES ('" . mysql_real_escape_string($pkgid) . "','" . mysql_real_escape_string($idA) . "');";
		mysql_query($sql);
	}

	// get idA
	$idA = db_get_message_id($msgA, $langA);
	if ($idA == -1) {
		return;
	}

	// if msgB exists, get idB
	if (!db_message_present($msgB, $langB)) {
		// insert msgB
		$sql = "INSERT INTO " . mysql_real_escape_string($langB) . " (message) VALUES ('" . mysql_real_escape_string($msgB) . "');";
		mysql_query($sql);

		// get idB
		$idB = db_get_message_id($msgB, $langB);
		if ($idB == -1) {
			return;
		}

		// insert pkg_langB 
		$sql = "INSERT INTO pkg_" . mysql_real_escape_string($langB) . " VALUES ('" . mysql_real_escape_string($pkgid) . "','" . mysql_real_escape_string($idB) . "');";
		mysql_query($sql);
	}

	// get idB
	$idB = db_get_message_id($msgB, $langB);
	if ($idB == -1) {
		return;
	}

	// insert into langA_langB and langB_langA
	$sql = "INSERT INTO " . mysql_real_escape_string($langA) . "_" . mysql_real_escape_string($langB) . " VALUES ('" . $idA . "','" . $idB . "');";
	mysql_query($sql);
	$sql = "INSERT INTO " . mysql_real_escape_string($langB) . "_" . mysql_real_escape_string($langA) . " VALUES ('" . $idB . "','" . $idA . "');";
	mysql_query($sql);

	print "INSERT COMPLETE\n";
}

/**
 * PO File Parser
 *
 * This parser doesn't fully comply with the 'standard' yet.
 * See http://www.gnu.org/software/gettext/manual/gettext.html#PO-Files
 *
 * This parser doesn't handle plural translations, nor does it inspect
 * any comments, extracted comments, flags, references, nor previous
 * untranslated strings. However, it does handle multi-line strings
 * and regular msgid's and msgstr's. It is enough for the proof-of-concept
 * version.
 */
function tt_parse_po($filename) {
	$translations = array();
	$contents = file_get_contents($filename);

	if (!$contents) {
		return $translations;
	}

	// figure out the language in a very dumb way.
	$path_parts = preg_split("/\//", $filename);
	$file_parts = preg_split("/\./", $path_parts[count($path_parts)-1]);
	$locale = $file_parts[0];

	// remove blank lines and comments
	$lines = preg_split("/\n/", $contents);
	$contents = "";	
	for ($i = 0; $i < count($lines); $i++) {
		$line = trim($lines[$i]);

		if ($line == "" || $line[0] == '#') {
			array_splice($lines, $i, 1);
			$i = $i - 1;
		} else {
			$contents .= $line . "\n";
		}
	}

	// Remove new line characters from translation strings.
	$contents = preg_replace("/\\\\n /", "", $contents);
	$contents = preg_replace("/ \\\\n/", "", $contents);
	$contents = preg_replace("/\\\\n/", "", $contents);

	// Remove return characters from translation strings.
	$contents = preg_replace("/\\\\r /", "", $contents);
	$contents = preg_replace("/ \\\\r/", "", $contents);
	$contents = preg_replace("/\\\\r/", "", $contents);

	// Remove tab characters from translation strings.
	$contents = preg_replace("/\\\\t /", "", $contents);
	$contents = preg_replace("/ \\\\t/", "", $contents);
	$contents = preg_replace("/\\\\t/", "", $contents);

	// join multi-line strings
	$contents = preg_replace("/\"\n\"/", "", $contents);
	$lines = preg_split("/\\n/", $contents);

	$i = 0;

	// XXX: this is not a good implementation -- re-write it!
	do {
		while (!preg_match("/^msgid /", $lines[$i]) && $i < count($lines)) {
			$i++;
		}
		if ($i >= count($lines)) {
			break;
		}
		$msgid = substr($lines[$i], strlen("msgid \""));
		$msgid = substr($msgid, 0, -1);

		// Remove double quotes from translation strings.
		$msgid = preg_replace("/\\\\\" /", "", $msgid);
		$msgid = preg_replace("/ \\\\\"/", "", $msgid);
		$msgid = preg_replace("/\\\\\"/", "", $msgid);

		// Remove quote from translation strings.
		$msgid = preg_replace("/\\\' /", "", $msgid);
		$msgid = preg_replace("/ \\\'/", "", $msgid);
		$msgid = preg_replace("/\\\'/", "", $msgid);

		// Remove slash from translation strings.
		$msgid = preg_replace("/\\\\ /", "", $msgid);
		$msgid = preg_replace("/ \\\\/", "", $msgid);
		$msgid = preg_replace("/\\\\/", "", $msgid);

		while (!preg_match("/^msgstr /", $lines[$i]) && $i < count($lines)) {
			$i++;
		}
		if ($i >= count($lines)) {
			break;
		}
		$msgstr = substr($lines[$i], strlen("msgstr \""));
		$msgstr = substr($msgstr, 0, -1);

		// Remove double quotes from translation strings.
		$msgstr = preg_replace("/\\\\\" /", "", $msgstr);
		$msgstr = preg_replace("/ \\\\\"/", "", $msgstr);
		$msgstr = preg_replace("/\\\\\"/", "", $msgstr);

		// Remove quote from translation strings.
		$msgstr = preg_replace("/\\\' /", "", $msgstr);
		$msgstr = preg_replace("/ \\\'/", "", $msgstr);
		$msgstr = preg_replace("/\\\'/", "", $msgstr);

		// Remove slash from translation strings.
		$msgstr = preg_replace("/\\\\ /", "", $msgstr);
		$msgstr = preg_replace("/ \\\\/", "", $msgstr);
		$msgstr = preg_replace("/\\\\/", "", $msgstr);

		if (strlen($msgid) != 0 && strlen($msgstr) != 0) {
			$t = new tt_translation();
			$t->msgid = trim($msgid);
			$t->msgstr = trim($msgstr);
			$t->locale = $locale;

			array_push($translations, $t);
		}


		$i++;
	} while ($i < count($lines));

	return $translations;
}

/**
 * Recusively scans $path looking for filenames that match $regex.
 */
function tt_find_files($path, $regex, $depth = 0) {
	$target = array();

	if (!is_dir($path)) {
		return $target;
	}

	$files = scandir($path);

	array_splice($files, 0, 2);

	$tabs = "";
	for ($i = 0; $i < $depth; $i++) {
		$tabs .= "\t";
	}

	foreach ($files as $file) {
		$fullpath = $path . '/' . $file;

		if (preg_match($regex, $file) && is_file($fullpath)) {
			array_push($target, $fullpath);
		}

		if (is_dir($fullpath)) {
			$result = tt_find_files($fullpath, $regex, $depth + 1);
			if (count($result) > 0) {
				$target = array_merge($target, $result);
			}
		}
	}

	return $target;
	
}

db_connect();

$sql = "CREATE TABLE IF NOT EXISTS `package` (id MEDIUMINT NOT NULL AUTO_INCREMENT, name CHAR(64) NOT NULL, version CHAR(16) NOT NULL, homepage CHAR(255) NOT NULL, download CHAR(255) NOT NULL, PRIMARY KEY (id) );";
mysql_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `license` (id MEDIUMINT NOT NULL AUTO_INCREMENT, name CHAR(64) NOT NULL, url CHAR(255) NOT NULL, PRIMARY KEY (id) );";
mysql_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS `package_license` (pkgid MEDIUMINT NOT NULL, licid MEDIUMINT NOT NULL, PRIMARY KEY (pkgid, licid) );";
mysql_query($sql);

$files = tt_find_files('gettext-0.17', "/^[a-zA-Z_]+\.po$/");

foreach ($files as $file) {
	$translations = tt_parse_po($file);

	foreach ($translations as $translation) {
		// echo "[en] " . $translation->msgid . "\n";
		// echo "[" . $translation->locale . "] " . $translation->msgstr . "\n";

		tt_insert_db($translation->msgid, "en", $translation->msgstr, $translation->locale);
	}
}

db_disconnect();

?>
